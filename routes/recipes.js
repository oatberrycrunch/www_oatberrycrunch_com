
var express = require('express');
var fs = require('fs');

var router = express.Router();

router.param('name', function (req, res, next, name) {
  req.recipe = require('../data/recipes/' + name + ".json");
  next();
});

router.get('/:name', function (req, res, next) {
  res.render(
    'recipe',
    {
      title: '{ oatberry crunch: ' + req.recipe.name + ' }',
      recipe:  req.recipe
    }
  );
});

module.exports = router;
