/*
Copyright (C) 2016  Andy Coder <andy@oatberrycrunch.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

(function () {
    "use strict";

    var s = 0,
        red = 128,
        green = 36,
        blue = 64,
        squares = [],
        numberOfSquares = 16,
        squareSizes = [0, 0, 0, 0],
        canvas,
        context,
        audioContext,
        audioLoopSource,
        audioLoopElement;

    window.onload = function () {
        // set up the audio loop
        audioContext = new (window.AudioContext || window.webkitAudioContext)();
        audioLoopElement = document.getElementById("loop");
        audioLoopSource = audioContext.createMediaElementSource(audioLoopElement);
        audioLoopSource.connect(audioContext.destination);
        audioLoopElement.loop = true;
        audioLoopElement.play();

        // size the canvas
        canvas = document.getElementById("canvas");
        context = canvas.getContext("2d");
        var viewWidth = Math.max(
                document.documentElement.clientWidth, window.innerWidth || 0
            ),
            viewHeight = Math.max(
                document.documentElement.clientHeight, window.innerHeight || 0
            );
        s = Math.min(viewWidth, viewHeight);
        canvas.width = s;
        canvas.height = s;

        canvas.onclick = function () {
            if (!audioLoopElement.paused) {
                audioLoopElement.pause();
            } else {
                audioLoopElement.play();
            }
        };

        squareSizes[0] = s * 0.02;
        squareSizes[1] = s * 0.04;
        squareSizes[2] = s * 0.08;
        squareSizes[3] = s * 0.16;

        window.requestAnimationFrame(run);
    };

    window.onresize = function () {
        // size the canvas
        canvas = document.getElementById("canvas");
        context = canvas.getContext("2d");
        var viewWidth = Math.max(
                document.documentElement.clientWidth, window.innerWidth || 0
            ),
            viewHeight = Math.max(
                document.documentElement.clientHeight, window.innerHeight || 0
            );
        s = Math.min(viewWidth, viewHeight);
        canvas.width = s;
        canvas.height = s;

        squareSizes[0] = s * 0.02;
        squareSizes[1] = s * 0.04;
        squareSizes[2] = s * 0.08;
        squareSizes[3] = s * 0.16;
        squares = [];
    };

    function update(time) {
        if (squares.length === 0) {
            for (var i = 0; i <= numberOfSquares; i++) {
                var size = squareSizes[randomIndex()];
                squares.push({
                    x: randomPosition(size),
                    y: randomPosition(size),
                    size: size,
                    alpha: randomAlpha(),
                    asc: true
                });
            }
            return;
        }
        for (var square in squares) {
            if (squares.hasOwnProperty(square)) {
                if (squares[square].alpha < 0.001) {
                    squares[square].x = randomPosition(squares[square].size);
                    squares[square].y = randomPosition(squares[square].size);
                    squares[square].asc = true;
                }
                if (squares[square].asc) {
                    squares[square].alpha += 1 / 120;
                    if (squares[square].alpha > 0.999) {
                        squares[square].alpha = 1;
                        squares[square].asc = false;
                    }
                } else {
                    squares[square].alpha -= 1 / 120;
                    if (squares[square].alpha < 0) {
                        squares[square].alpha = 0;
                    }
                }
            }
        }
    }

    function draw(time) {
        context.fillStyle = "#AAAAAA";
        context.fillRect(0, 0, s, s);
        for (var square in squares) {
            if (squares.hasOwnProperty(square)) {
                context.fillStyle = "rgba(" + red + "," + green + "," + blue + "," + squares[square].alpha + ")";
                context.fillRect(squares[square].x, squares[square].y, squares[square].size, squares[square].size);
            }
        }
    }

    function randomAlpha() {
        return Math.random();
    }

    function randomIndex() {
        return Math.floor(Math.random() * 3);
    }

    function randomPosition(sizeOfSquare) {
        return Math.floor(Math.random() * (s - sizeOfSquare) + 1);
    }

    function run(time) {
        update(time);
        draw(time);
        window.requestAnimationFrame(run);
    }
}());
