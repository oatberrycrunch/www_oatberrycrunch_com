/*
Copyright (C) 2016  Andy Coder <andy@oatberrycrunch.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

(function () {
    "use strict";

    var canvasWidth = 0,
        canvasHeight = 0,
        red = 128,
        green = 36,
        blue = 64,
        rows = [],
        speed = 0.015,
        speedFactor = 2500,
        numberOfRectangles = 16,
        numberOfColumns = 12,
        numberOfRows = 8,
        xScale = 0.08,
        yScale = 0.12,
        rectangleDimensions = [0, 0],
        canvas,
        context,
        audioContext,
        audioLoopSource,
        audioLoopElement;

    window.onload = function () {
        // set up the audio loop
        audioContext = new (window.AudioContext || window.webkitAudioContext)();
        audioLoopElement = document.getElementById("loop");
        audioLoopSource = audioContext.createMediaElementSource(audioLoopElement);
        audioLoopSource.connect(audioContext.destination);
        audioLoopElement.loop = true;
        audioLoopElement.play();

        // size the canvas
        canvas = document.getElementById("canvas");
        context = canvas.getContext("2d");
        var viewWidth = Math.max(
                document.documentElement.clientWidth, window.innerWidth || 0
            ),
            viewHeight = Math.max(
                document.documentElement.clientHeight, window.innerHeight || 0
            );
        if(viewHeight > viewWidth) {
            canvasHeight = viewHeight;
            canvasWidth = viewWidth;
        } else {
            canvasHeight = viewHeight;
            canvasWidth = canvasHeight;
        }
        canvas.width = canvasWidth;
        canvas.height = canvasHeight;

        rectangleDimensions[0] = canvasWidth * xScale;
        rectangleDimensions[1] = canvasHeight * yScale;

        canvas.onclick = function () {
            if (!audioLoopElement.paused) {
                audioLoopElement.pause();
            } else {
                audioLoopElement.play();
            }
        };

        window.requestAnimationFrame(run);
    };

    window.onresize = function () {
        // size the canvas
        canvas = document.getElementById("canvas");
        context = canvas.getContext("2d");
        var viewWidth = Math.max(
                document.documentElement.clientWidth, window.innerWidth || 0
            ),
            viewHeight = Math.max(
                document.documentElement.clientHeight, window.innerHeight || 0
            );
        if(viewHeight > viewWidth) {
            canvasHeight = viewHeight;
            canvasWidth = viewWidth;
        } else {
            canvasHeight = viewHeight;
            canvasWidth = canvasHeight;
        }
        canvas.width = canvasWidth;
        canvas.height = canvasHeight;

        rectangleDimensions[0] = canvasWidth * xScale;
        rectangleDimensions[1] = canvasHeight * yScale;
        rows = [];
    };

    function update(step) {
        if (rows.length === 0) {
            rows.push(produceARowOfRectangles());
        } else {
            for (var row in rows) {
                if (rows.hasOwnProperty(row)) {
                    for (var rectangle in rows[row]) {
                        if (rows[row].hasOwnProperty(rectangle)) {
                            if (rows[row][rectangle].y >= -1 * rectangleDimensions[1]) {
                                rows[row][rectangle].y -= step * speed * canvasHeight / speedFactor;
                            } else {
                                rows[row][rectangle].y = canvasHeight + rectangleDimensions[1];
                                rows[row][rectangle].x = (canvasWidth / numberOfColumns) * randomIndex();
                                rows[row][rectangle].alpha = randomAlpha();
                            }
                        }
                    }
                }
            }
            if (rows[0][0].y < ((numberOfRows - (rows.length - 1)) * (canvasHeight / numberOfRows))) {
                rows.push(produceARowOfRectangles());
            }
        }
    }

    function draw(step) {
        context.fillStyle = "#AAAAAA";
        context.fillRect(0, 0, canvasWidth, canvasHeight);
        for (var row in rows) {
            if (rows.hasOwnProperty(row)) {
                for (var rectangle in rows[row]) {
                    if (rows[row].hasOwnProperty(rectangle)) {
                        context.fillStyle = "rgba(" + red + "," + green + "," + blue + "," + rows[row][rectangle].alpha + ")";
                        context.fillRect(rows[row][rectangle].x, rows[row][rectangle].y, rectangleDimensions[0], rectangleDimensions[1]);
                    }
                }
            }
        }
    }

    function produceARowOfRectangles() {
        var rectangles = [];
        for (var i = 0; i <= numberOfRectangles; i++) {
            rectangles.push({
                x: (canvasWidth / numberOfColumns) * randomIndex(),
                y: canvasHeight + rectangleDimensions[1],
                alpha: randomAlpha()
            });
        }
        return rectangles;
    }

    function randomAlpha() {
        return Math.random();
    }

    function randomIndex() {
        return Math.floor(Math.random() * numberOfColumns);
    }

    var firstTime = window.performance.now();
    var endStep = 0;

    function run(time) {
        if (endStep == 0) {
            var step = time - firstTime;
            update(step);
            draw(step);
            if ((step) * speed * canvasHeight / speedFactor > canvasHeight / 1.5) {
                endStep = step;
            }
        } else {
            update(endStep);
            draw(endStep);
        }
        window.requestAnimationFrame(run);
    }
}());
